#!/usr/bin/env bash

set -e

# Starts test suite
#
# Usage:  test [<suite>] [<option>]
#
# Examples:
#     run_tests                 - starts all tests suites


echo "Run Api Test Suite"

[ -z "$COMPOSE_FILE" ] && COMPOSE_FILE=$(cat .env | grep COMPOSE_FILE | cut -d = -f 2)
COMPOSE_FILE="$COMPOSE_FILE:docker/docker-compose.test.yml";
export COMPOSE_FILE=$COMPOSE_FILE

docker-compose up -d
cmd="php /app/cli.php migrate"
passed=0;
docker-compose run --rm php bash -c "$cmd" || passed=$?
docker-compose run --rm php bash -c "php -d \"memory_limit=512M\" vendor/bin/behat -vvv"

docker-compose kill database_test
docker-compose rm -f database_test
docker-compose kill php
docker-compose rm -f php
docker-compose kill front
docker-compose rm -f front