import { rangeProvider } from '../api/providers';
import dateFormat from 'dateformat';
import * as constants from './constants';

export function loadRanges() {
    return async dispatch => {
        let ranges = await rangeProvider.list();

        dispatch(setRanges(ranges));

        return ranges;
    };

}

export function setRanges(ranges) {
    return {
        type: constants.SET_RANGES,
        ranges
    };
}

export function addRange(range) {
    return {
        type: constants.ADD_RANGE,
        range
    }
}

export function setSelectedRanges(ranges) {
    return {
        type: constants.SET_SELECTED_RANGES,
        ranges
    }
}

export function removeSelectedRanges() {
    const filterRanges = (ranges, ids) => {
        return ranges.filter(range => !ids.includes(range.id));
    };

    return async (dispatch, getState) => {
        let state = getState();

        await rangeProvider.remove(state.selectedRanges);

        dispatch(setRanges(
            filterRanges(state.ranges, state.selectedRanges)
        ));
        dispatch(setSelectedRanges([]));
    };
}

export function storeRange({from, to}) {
    const format = (date) => dateFormat(date, 'yyyy-mm-dd HH:MM:ss');

    return async dispatch => {
        let range = await rangeProvider.store({
            from: format(from),
            to: format(to)
        });

        dispatch(addRange(range));

        return range;
    }
}