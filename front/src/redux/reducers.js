import * as constans from './constants';

const defaultState = {
    ranges: [],
    selectedRanges: []
};

const appReducer = (state = defaultState, action) => {
    switch (action.type) {
        case constans.SET_RANGES:
            return {
                ...state,
                ranges: action.ranges,
            };
        case constans.ADD_RANGE:
            return {
                ...state,
                ranges: [...state.ranges, action.range]
            };
        case constans.SET_SELECTED_RANGES:
            return {
                ...state,
                selectedRanges: action.ranges
            };
        default:
            return state;
    }
};

export default appReducer;