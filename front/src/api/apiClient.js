import axios from 'axios';

class ApiClient {
    constructor({ apiUrl }) {
        this.apiUrl = apiUrl;
        this.axios = axios;
    }

    async get(endpoint, params = {}, opts = {}) {
        return await this.send(
            Object.assign({ method: "get", endpoint: endpoint, params: params }, opts)
        )
    }

    async post(endpoint, payload = {}, opts = {}) {
        return await this.send(
            Object.assign({ method: "post", endpoint: endpoint, payload: payload }, opts)
        )
    }

    async put(endpoint, payload = {}, opts = {}) {
        return await this.send(
            Object.assign({ method: "put", endpoint: endpoint, payload: payload }, opts)
        )
    }

    async patch(endpoint, payload = {}, opts = {}) {
        return await this.send(
            Object.assign({ method: "patch", endpoint: endpoint, payload: payload }, opts)
        )
    }
    async delete(endpoint, payload = {}, opts = {}) {
        return await this.send(
            Object.assign({ method: "delete", endpoint: endpoint, payload: payload }, opts)
        )
    }

    async send(request) {
        const {
            method = "get",
            endpoint,
            payload = {},
            headers = {},
            params
        } = request;

        const response = await this.axios({
            method,
            headers,
            params,
            url: endpoint,
            data: payload,
            baseURL: this.apiUrl
        });

        return response.data;
    }
}

export default ApiClient;

export const apiHost = process.env.REACT_APP_API_HOST || 'http://megaphone.docker/api';
export const client = new ApiClient( {apiUrl: apiHost});