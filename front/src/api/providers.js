import { client } from "./apiClient";
import Range from "./models";

const mapper = (item) => new Range(item);

class RangeProvider {
    constructor(client) {
        this.client = client;

        this.endpoint = '/range';
    }

    async list() {
        let data = await this.client.get(this.endpoint);

        data = data || [];
        return data.map(mapper);
    }

    async remove(ids = []) {
        if (0 === ids.length) {
            return;
        }

        await this.client.delete(this.endpoint, { ids })
    }

    async store(range) {
        let data = await this.client.post(this.endpoint, range);

        return mapper(data);
    }
}

export const rangeProvider = new RangeProvider(client);