export default class Range {
    constructor({id, from, to}) {
        this.id = id;
        this._id = id;
        this.from = from;
        this.to = to;
    }
}