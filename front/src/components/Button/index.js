import React, { Component } from 'react';
import PropTypes from "prop-types";

import './index.css';

export function buttonHOC(Title) {
    let Button = class extends Component {
        render() {
            let className = this.props.className || '';
            if (!!this.props.isDisabled) {
                className += ' disabled';
            }

            return (
                <button className={className + ' btn'} onClick={() => this.props.handleClick()}>
                    <Title/>
                </button>
            );
        }
    };

    Button.propTypes = {
        className: PropTypes.string,
        handleClick: PropTypes.func.isRequired,
        isDisabled: PropTypes.bool
    };

    return Button;
}
