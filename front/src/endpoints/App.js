import React, { Component, Fragment } from 'react';
import AddRange from './AddRange/index'
import Ranges from './Ranges/index';
import './App.css';

class App extends Component {
    render() {
        return (
            <Fragment>
                <div className="Header">
                    <h1>Megaphone</h1>
                </div>
                <AddRange />
                <Ranges />
            </Fragment>
        );
    }
}

export default App;
