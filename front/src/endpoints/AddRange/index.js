import React, { Component } from 'react';
import DatetimeRangePicker from 'react-datetime-range-picker';

import { storeRange } from './../../redux/actions';

import { FaPlus } from "react-icons/fa";
import { buttonHOC } from '../../components/Button';

import './index.css';
import connect from "react-redux/es/connect/connect";

const AddButton = buttonHOC(FaPlus);

class AddRange extends Component {
    constructor(props) {
        super(props);

        this.state = {
            start: new Date(),
            end: new Date()
        };
    }

    handleAddButton = () => {
        this.props.storeRange({
            from: this.state.start,
            to: this.state.end
        })
    };

    render() {
        return (
            <div className="AddForm">
                <h2>Add Date Range</h2>
                <div className="form-block">
                    <DatetimeRangePicker
                        className="datetime-range"
                        startDate={this.state.from}
                        endDate={this.state.to}
                        onChange={range => this.setState(range)}
                    />
                    <AddButton className="add-btn" handleClick={this.handleAddButton}/>
                </div>
            </div>
        );
    }
}

export default connect(
    state => state,
    { storeRange }
)(AddRange);
