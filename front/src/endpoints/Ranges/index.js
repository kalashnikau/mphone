import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { loadRanges, setSelectedRanges, removeSelectedRanges } from "../../redux/actions";
import { connect } from 'react-redux';

import ReactTable from "react-table";
import checkboxHOC from "react-table/lib/hoc/selectTable";
import "react-table/react-table.css";

import { FaTrashAlt } from "react-icons/fa";
import { buttonHOC } from '../../components/Button';

import './index.css';

const CheckboxTable = checkboxHOC(ReactTable);
const RemoveButton = buttonHOC(FaTrashAlt);

class Ranges extends Component {
    constructor(props) {
        super(props);

        this.state = {
            columns: [
                {accessor: 'from', Header: 'Start Date'},
                {accessor: 'to', Header: 'End Date'},
            ]
        }
    }

    componentWillMount() {
        this.props.loadRanges();
    }

    toggleSelection = (key) => {
        let selection = [...this.props.selectedRanges];

        const keyIndex = selection.indexOf(key);
        if (keyIndex >= 0) {
            selection = [
                ...selection.slice(0, keyIndex),
                ...selection.slice(keyIndex + 1)
            ];
        } else {
            selection.push(key);
        }

        this.props.setSelectedRanges(selection);
    };

    toggleAll = () => {
        const selection = [];
        if (!this.isSelectAll()) {
            const wrappedInstance = this.checkboxTable.getWrappedInstance();
            const currentRecords = wrappedInstance.getResolvedState().sortedData;

            currentRecords.forEach(item => {
                selection.push(item._original.id);
            });
        }

        this.props.setSelectedRanges(selection);
    };

    isSelectAll = () => {
        return this.props.selectedRanges.length === this.props.ranges.length;
    };

    isSelected = key => {
        return this.props.selectedRanges.includes(key);
    };

    render() {
        const { isSelected, toggleSelection, toggleAll, isSelectAll } = this;
        const { columns } = this.state;

        const checkboxProps = {
            selectAll: isSelectAll(),
            selectType: "checkbox",
            isSelected,
            toggleSelection,
            toggleAll
        };

        return (
            <div className="Ranges">
                <RemoveButton
                    text={FaTrashAlt}
                    className="remove-btn"
                    handleClick={this.props.removeSelectedRanges}
                    isDisabled={0 === this.props.selectedRanges.length}
                />
                <CheckboxTable
                    filterable
                    ref={r => (this.checkboxTable = r)}
                    data={this.props.ranges}
                    columns={columns}
                    defaultPageSize={10}
                    className="-striped -highlight"
                    {...checkboxProps}
                />
            </div>
        );
    }
}

Ranges.propTypes = {
    ranges: PropTypes.array.isRequired,
    selectedRanges: PropTypes.array
};

export default connect(
    state => state,
    { loadRanges, setSelectedRanges, removeSelectedRanges }
)(Ranges);
