<?php

namespace Feature\Helper;

use Megaphone\Model\DateRange;
use Illuminate\Database\Capsule\Manager as DB;

trait RangeTrait
{
    private $ranges = [];

    /**
     * @Given /^The system has "([^"]*)" ranges$/
     */
    public function systemHasRanges($count)
    {
        DB::table('ranges')->truncate();

        foreach (range(1, $count) as $i) {
            $range = new DateRange([
                'from' => new \DateTime(),
                'to' => new \DateTime("+{$i} day")
            ]);
            $this->ranges[$i] = $range;

            $range->save();
        }
    }

    /**
     * @Given /^The system has range with "([^"]*)" and "([^"]*)" dates$/
     */
    public function systemHasRangeWith($from, $to)
    {
        DB::table('ranges')->truncate();

        $range = new DateRange([
            'from' => new \DateTime($from),
            'to' => new \DateTime($to)
        ]);

        $range->save();
    }
}