<?php

use Behat\Behat\Context\Context;
use Pavlakis\Slim\Behat\Context\App;
use Pavlakis\Slim\Behat\Context\KernelAwareContext;
use Feature\Helper\RangeTrait;

class FeatureContext implements Context, KernelAwareContext
{
    use App;
    use RangeTrait;
}
