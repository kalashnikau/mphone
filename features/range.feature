Feature: Date Range

  As a behat user
  I want to test restful api of the date ranges

  Background:
    Given The system has "10" ranges
    When I add "Content-Type" header equal to "application/json"

  Scenario: I want to get list of ranges
    When I send a GET request to "api/range"
    Then the response should be in JSON
    And the response status code should be 200
    And the JSON node "root[0].id" should exist
    And the JSON node "root[0].from" should exist
    And the JSON node "root[0].to" should exist

  Scenario: I want to store range
    When I send a POST request to "api/range" with body:
      """
      {
        "from": "2005-08-15 15:52:01",
	    "to": "2005-08-15 15:52:01"
      }
      """
    Then the response should be in JSON
    And the response status code should be 201
    And the JSON node "root.id" should exist
    And the JSON node "root.from" should exist
    And the JSON node "root.to" should exist

  Scenario: I want to see validation error when i send incorrect data to store range
    When I send a POST request to "api/range" with body:
      """
      {
        "from": "2005-08-15 15:52:01"
      }
      """
    Then the response should be in JSON
    And the response status code should be 422
    And the JSON node "root.status_code" should exist
    And the JSON node "root.message" should exist
    And the JSON node "root.errors" should exist

  Scenario: I want to delete ranges from system
    When I send a DELETE request to "api/range" with body:
      """
      {
        "ids": [1, 2, 3, 4]
      }
      """
    Then the response should be empty
    And the response status code should be 204


  Scenario: I want to check the date in the interval
    Given The system has range with "2018-08-15 00:00:00" and "2018-08-17 00:00:00" dates
    When I send a GET request to "api/range/check?test=2018-08-16 00:00:00"
    Then the response should be in JSON
    And the response status code should be 200
    And the JSON node "root.success" should exist
    And the JSON node "root.url" should exist
    And the JSON node "root.now" should exist
    And the JSON node "root.success" should be equal to true


