#!/usr/bin/env php
<?php

use Illuminate\Database\Capsule\Manager as Capsule;

require __DIR__ . '/vendor/autoload.php';

class Cli
{
    /**
     * @var array
     */
    private $args;

    /**
     * @var Capsule
     */
    private $capsule;

    public function __construct(array $args)
    {
        $this->args = $args;

        $this->capsule = new Capsule();
        $this->capsule->addConnection($this->args['settings']['db']);
        $this->capsule->setAsGlobal();
        $this->capsule->bootEloquent();
    }

    private function help()
    {
        echo "\n";
        echo "syntaxis: php cli <command> [<args>]" . PHP_EOL;
        echo PHP_EOL;
        echo "Commands: \n";
        echo "php cli --help                  -->   Displays the help menu." . PHP_EOL;
        echo "php cli migrate                 -->   Migrate the database." . PHP_EOL;
        echo PHP_EOL;
    }

    public function exec()
    {
        if (count($this->args) <= 1) {
            $this->help();
        } else {
            switch ($this->args[1]) {
                case "migrate":
                    $this->runMigrations();
                    break;
                case "help":
                case "--help":
                    $this->help();
                    break;
            }
        }
    }

    private function runMigrations()
    {
        $files = glob($this->args['settings']['migration_path'] . "/*.php");

        foreach ($files as $file) {
            $class = $this->args['settings']['migration_class_prefix'] . basename($file, '.php');
            /** @var \Megaphone\Database\Migration $obj */
            $obj = new $class();
            $obj->run();
        }
    }
}

$settings = require __DIR__ . '/app/settings.php';
$novice = new Cli($settings + $argv);
$novice->exec();