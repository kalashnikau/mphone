<?php

use \Slim\Container;
use \Symfony\Component\Validator\Validation;

// DIC configuration

$container = $app->getContainer();

// monolog
$container['logger'] = function (Container $container) {
    $settings = $container->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));

    return $logger;
};

// Service factory for the ORM
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();
$container['db'] = function () use ($capsule) {
    return $capsule;
};

$container['validator'] = function () {
    return Validation::createValidator();
};

$container['errorHandler'] = function (Container $container) {
    return new \Megaphone\ErrorHandler($container->get('logger'));
};

// Transformers
$container[\Megaphone\Http\Transformer\RangeTransformer::class] = function () {
    return new \Megaphone\Http\Transformer\RangeTransformer();
};

$container[\Megaphone\Http\Transformer\CheckResultTransformer::class] = function () {
    return new \Megaphone\Http\Transformer\CheckResultTransformer();
};

// Action factories
$container[\Megaphone\Http\Action\CreateRangeAction::class] = function (Container $container) {
    return new \Megaphone\Http\Action\CreateRangeAction(
        $container->get('validator'),
        $container->get(\Megaphone\Http\Transformer\RangeTransformer::class)
    );
};

$container[\Megaphone\Http\Action\GetListRangesAction::class] = function (Container $container) {
    return new \Megaphone\Http\Action\GetListRangesAction(
        $container->get(\Megaphone\Http\Transformer\RangeTransformer::class)
    );
};

$container[\Megaphone\Http\Action\CheckDateAction::class] = function (Container $container) {
    return new \Megaphone\Http\Action\CheckDateAction(
        $container['settings']['megaphone_url'],
        $container->get(\Megaphone\Http\Transformer\CheckResultTransformer::class)
    );
};