<?php

$app->group('/api', function () use ($app) {
    $app->group('/range', function () use ($app) {
        $app->get('', \Megaphone\Http\Action\GetListRangesAction::class);
        $app->post('', \Megaphone\Http\Action\CreateRangeAction::class);
        $app->delete('', \Megaphone\Http\Action\BulkDeleteRangesAction::class);

        $app->get('/check', \Megaphone\Http\Action\CheckDateAction::class);
    });
});

