<?php
return [
    'settings' => [
        'displayErrorDetails' => ('dev' === getenv('APP_ENV')),
        'addContentLengthHeader' => false,

        'migration_path' => '/app/src/Database/Migration',
        'migration_class_prefix' => 'Megaphone\\Database\\Migration\\',

        'megaphone_url' => getenv('MEGAPHONE_URL'),

        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
            'cache_path' => __DIR__ . '/../var/cache/'
        ],

        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../var/logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        'db' => [
            'driver' => 'mysql',
            'host' => getenv('DB_HOST') ?? 'localhost',
            'database' => getenv('DB_NAME') ?? 'database',
            'username' => getenv('DB_USER') ?? 'user',
            'password' => getenv('DB_PASS') ?? 'password',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ]
    ],
];
