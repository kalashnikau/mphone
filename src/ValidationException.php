<?php

namespace Megaphone;

use Symfony\Component\Validator\ConstraintViolation;

class ValidationException extends \Exception implements ResponseException
{
    private $errors;

    public function __construct(\Traversable $errors, string $message = 'Invalid data in request body', int $code = 422)
    {
        parent::__construct($message, $code);

        $this->errors = $errors;
    }

    /**
     * @inheritdoc
     */
    public function getResponseBody(): array
    {
        return [
            'status_code' => $this->code,
            'message' => $this->message,
            'errors' => array_map(function (ConstraintViolation $violation) {
                return [
                    'path' => $violation->getPropertyPath(),
                    'message' => $violation->getMessage(),
                ];
            }, iterator_to_array($this->errors)),
        ];
    }
}