<?php

namespace Megaphone;

use Monolog\Logger;
use Slim\Http\Request;
use Slim\Http\Response;

class ErrorHandler
{
    /**
     * @var Logger
     */
    private $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param \Exception $exception
     *
     * @return Response
     */
    public function __invoke(Request $request, Response $response, \Exception $exception): Response
    {
        if ($exception instanceof ResponseException) {
            return $response->withJson($exception->getResponseBody(), $exception->getCode());
        }

        $this->logger->addError($exception->getTraceAsString());

        return $response;
    }
}