<?php

namespace Megaphone\Http\Transformer;

use Megaphone\Model\CheckResult;

class CheckResultTransformer implements Transformer
{
    use DateFormatter;

    /**
     * @param CheckResult $model
     *
     * @return array
     */
    public function transform($model): array
    {
        return [
            'success' => $model->isSuccess(),
            'url' => $model->url(),
            'now' => $this->formatDate($model->now())
        ];
    }
}