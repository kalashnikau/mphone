<?php

namespace Megaphone\Http\Transformer;

interface Transformer
{
    /**
     * Transform entity
     *
     * @param mixed $model
     * @return array
     */
    public function transform($model): array;
}