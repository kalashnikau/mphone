<?php

namespace Megaphone\Http\Transformer;

trait DateFormatter
{
    public function formatDate(\DateTime $date): string
    {
        return $date->format('Y-m-d H:i:s');
    }
}