<?php

namespace Megaphone\Http\Transformer;

use Megaphone\Model\DateRange;

class RangeTransformer implements Transformer
{
    use DateFormatter;

    /**
     * @param DateRange $model
     *
     * @return array
     */
    public function transform($model): array
    {
        return [
            'id' => $model->getAttribute('id'),
            'from' => $this->formatDate($model->getAttribute('from')),
            'to' => $this->formatDate($model->getAttribute('to'))
        ];
    }
}