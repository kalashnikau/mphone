<?php

namespace Megaphone\Http\Action;

use Megaphone\Http\Transformer\CheckResultTransformer;
use Megaphone\Model\CheckResult;
use Megaphone\Model\DateRange;
use Slim\Http\Request;
use Slim\Http\Response;

class CheckDateAction extends AbstractAction
{
    public const BLANK_URL = 'about:blank';

    /**
     * @var string
     */
    private $url;

    /**
     * @var CheckResultTransformer
     */
    private $transformer;

    public function __construct(string $url, CheckResultTransformer $transformer)
    {
        $this->url = $url;
        $this->transformer = $transformer;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $now = new \DateTime($request->getParam('test'));

        $result = $this->isInInterval($now)
            ? new CheckResult($now, $this->url, true)
            : new CheckResult($now, self::BLANK_URL, false);

        return $this->item($response, $result, $this->transformer);
    }

    /**
     * @param \DateTime $date
     * @return bool
     */
    private function isInInterval(\DateTime $date): bool
    {
        $checkDate = $date->format('Y-m-d H:i:s');
        $count = DateRange::query()
            ->where('from', '<=', $checkDate)
            ->where('to', '>', $checkDate)
            ->count();

        return (0 !== $count);
    }
}