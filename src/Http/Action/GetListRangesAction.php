<?php

namespace Megaphone\Http\Action;


use Megaphone\Http\Transformer\RangeTransformer;
use Megaphone\Model\DateRange;
use Slim\Http\Request;
use Slim\Http\Response;

class GetListRangesAction extends AbstractAction
{
    /**
     * @var RangeTransformer
     */
    private $transformer;

    public function __construct(RangeTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     *
     * @return Response
     */
    public function __invoke(Request $request, Response $response, $args)
    {
        return $this->list(
            $response,
            DateRange::all()->all(),
            $this->transformer
        );
    }
}