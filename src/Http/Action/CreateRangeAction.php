<?php

namespace Megaphone\Http\Action;

use Megaphone\Http\Transformer\RangeTransformer;
use Megaphone\Model\DateRange;
use Megaphone\ValidationException;
use Slim\Http\Request;
use Slim\Http\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class CreateRangeAction extends AbstractAction
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var RangeTransformer
     */
    private $transformer;

    public function __construct(ValidatorInterface $validator, RangeTransformer $transformer)
    {
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     *
     * @return Response
     *
     * @throws ValidationException
     */
    public function __invoke(Request $request, Response $response, $args)
    {
        $this->validateRequest($request);

        $range = $this->store($request);

        return $this->created($response, $range, $this->transformer);

    }

    /**
     * @param Request $request
     *
     * @throws ValidationException
     */
    private function validateRequest(Request $request)
    {
        $errors = $this->validator->validate($request->getParsedBody(), new Assert\Collection([
            'from' => [
                new Assert\NotBlank(),
                new Assert\DateTime()
            ],
            'to' => [
                new Assert\NotBlank(),
                new Assert\DateTime()
            ]
        ]));

        if (0 !== count($errors)) {
            throw new ValidationException($errors);
        }
    }

    /**
     * Store data
     *
     * @param Request $request
     *
     * @return DateRange
     */
    private function store(Request $request): DateRange
    {
        $data = $request->getParsedBody();
        $range = new DateRange([
            'from' => new \DateTime($data['from']),
            'to' => new \DateTime($data['to'])
        ]);

        $range->save();

        return $range;
    }
}