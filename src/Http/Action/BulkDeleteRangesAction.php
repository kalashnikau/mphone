<?php

namespace Megaphone\Http\Action;

use Megaphone\Model\DateRange;
use Slim\Http\Request;
use Slim\Http\Response;

class BulkDeleteRangesAction extends AbstractAction
{
    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     *
     * @return BulkDeleteRangesAction
     */
    public function __invoke(Request $request, Response $response, $args)
    {
        DateRange::destroy(
            $this->prepareIds($request)
        );

        return $this->deleted($response);
    }

    /**
     * @param Request $request
     * @return array
     */
    private function prepareIds(Request $request): array
    {
        $body = $request->getParsedBody();

        return $body['ids'] ?? [];
    }
}