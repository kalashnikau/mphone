<?php

namespace Megaphone\Http\Action;

use Megaphone\Http\Transformer\Transformer;
use Slim\Http\Response;

abstract class AbstractAction
{
    public const HTTP_OK = 200;
    public const HTTP_CREATED = 201;
    public const HTTP_NO_CONTENT = 204;

    /**
     * @param Response $response
     * @param $entity
     * @param Transformer $transformer
     *
     * @return Response
     */
    protected function item(Response $response, $entity, Transformer $transformer): Response
    {
        return $response->withJson(
            $this->transformItem($entity, $transformer)
        );
    }

    /**
     * @param Response $response
     * @param array $data
     * @param Transformer $transformer
     *
     * @return Response
     */
    protected function list(Response $response, array $data, Transformer $transformer): Response
    {
        return $response->withJson(array_map(function ($item) use ($transformer) {
            return $this->transformItem($item, $transformer);
        }, $data));
    }

    /**
     * @param Response $response
     * @param mixed $entity
     * @param Transformer $transformer
     *
     * @return Response
     */
    protected function created(Response $response, $entity, Transformer $transformer): Response
    {
        return $response->withJson(
            $this->transformItem($entity, $transformer),
            201
        );
    }

    protected function deleted(Response $response)
    {
        return $response->withStatus(self::HTTP_NO_CONTENT);
    }

    /**
     * Transform item
     *
     * @param mixed $entity
     * @param Transformer $transformer
     *
     * @return array
     */
    private function transformItem($entity, Transformer $transformer)
    {
        return $transformer->transform($entity);
    }
}