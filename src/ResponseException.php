<?php

namespace Megaphone;

interface ResponseException
{
    /**
     * Get response body
     *
     * @return array
     */
    public function getResponseBody(): array;
}