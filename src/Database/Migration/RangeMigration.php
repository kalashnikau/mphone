<?php

namespace Megaphone\Database\Migration;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;
use Megaphone\Database\Migration;

class RangeMigration implements Migration
{
    private const TABLE_NAME = 'ranges';

    public function run()
    {
        if (!Capsule::schema()->hasTable(self::TABLE_NAME)) {
            Capsule::schema()->create(self::TABLE_NAME, function (Blueprint $table) {
                $table->increments('id');
                $table->dateTimeTz('from');
                $table->dateTimeTz('to');
                $table->timestamps();
            });
        }
    }
}