<?php

namespace Megaphone\Database;

interface Migration
{
    /**
     * Run migration
     *
     * @return void
     */
    public function run();
}