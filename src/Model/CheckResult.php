<?php

namespace Megaphone\Model;

class CheckResult
{
    /**
     * @var bool
     */
    private $success;

    /**
     * @var string
     */
    private $url;

    /**
     * @var \DateTime
     */
    private $now;

    public function __construct(\DateTime $now, string $url, bool $success)
    {
        $this->url = $url;
        $this->now = $now;
        $this->success = $success;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return string
     */
    public function url(): string
    {
        return $this->url;
    }

    /**
     * @return \DateTime
     */
    public function now(): \DateTime
    {
        return $this->now;
    }
}