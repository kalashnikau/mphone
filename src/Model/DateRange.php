<?php

namespace Megaphone\Model;

use Illuminate\Database\Eloquent\Model;

class DateRange extends Model
{
    protected $table = 'ranges';

    protected $fillable = ['from', 'to'];

    protected $dates = ['from', 'to'];
}